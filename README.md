# leet-code

👨‍🦲 Solutions of LeetCode.

Created by : Mr Dk.

2019 / 08 / 01 @Nanjing, Jiangsu, China

---

## About

The way to become bareheaded. 👨‍🦲

## Compile and Run

```bash
make SRC=1.TwoSum.cpp
```

```bash
make run
```

```bash
make clean
```

## LeetCode Collection

| #     | Problem                                        | Difficulty | Link | 
| ----  | ----                                           | ----       | ---- |
| 1     | Two Sum                                        | Easy       | [C++](1.TwoSum.cpp) |
| 2     | Add Two Numbers                                | Medium     | [C++](2.AddTwoNumbers.cpp) |
| 3     | Longest Substring Without Repeating Characters | Medium     | [C++](3.LongestSubstringWithoutRepeatingCharacters.cpp) |
| 4     | Median of Two Sorted Arrays                    | Hard       | [C++](4.MedianOfTwoSortedArrays.cpp) |
| 5     | Longest Palindromic Substring                  | Medium     | [C++](5.LongestPalindromicSubstring.cpp) |
| 6     | ZigZag Conversion                              | Medium     | [C++](6.ZigZagConversion.cpp) |
| 8     | String to Integer (atoi)                       | Medium     | [C++](8.StringToInteger.cpp) |
| 11    | Container with Most Water                      | Medium     | [C++](11.ContainerWithMostWater.cpp) |
| 12    | Integer to Roman                               | Medium     | [C++](12.IntegerToRoman.cpp) |
| 15    | 3Sum                                           | Medium     | [C++](15.3Sum.cpp) |
| 16    | 3Sum Closest                                   | Medium     | [C++](16.3SumClosest.cpp) |
| 17    | Letter Combinations of a Phone Number          | Medium     | [C++](17.LetterCombinationsOfAPhoneNumber.cpp) |
| 18    | 4Sum                                           | Medium     | [C++](18.4Sum.cpp) |
| 31    | Next Permutation                               | Medium     | [C++](31.NextPermutation.cpp) |
| 34    | Find First and Last Position of Element in Sorted Array | Medium | [C++](34.FindFirstAndLastPositionOfElementInSortedArray.cpp) |
| 48    | Rotate Image                                   | Medium     | [C++](48.RotateImage.cpp) |
| 49    | Group Anagrams                                 | Medium     | [C++](49.GroupAnagrams.cpp) |
| 50    | Pow(x, n)                                      | Medium     | [C++](Offer16.MyPow.cpp) |
| 62    | Unique Paths                                   | Medium     | [C++](62.UniquePaths.cpp) |
| 79    | Word Search                                    | Medium     | [C++](79.WordSearch.cpp) |
| 85    | Maximal Rectangle                              | Hard       | [C++](85.MaximalRectangle.cpp) |
| 103   | Binary Tree Zigzag Level Order Traversal       | Medium     | [C++](103.BinaryTreeZigzagLevelOrderTraversal.cpp) |
| 105   | Construct Binary Tree from Preorder and Inorder Traversal   | Medium     | [C++](Offer07.ConstructBinaryTreeFromPreorderAndInorderTraversal.cpp) |
| 118   | Pascal's Triangle                              | Easy       | [C++](118.PascalsTriangle.cpp) |
| 134   | Gas Station                                    | Medium     | [C++](134.GasStation.cpp) |
| 135   | Candy                                          | Hard       | [C++](135.Candy.cpp) |
| 138   | Copy List with Random Pointer                  | Medium     | [C++](Offer35.CopyListWithRandomPointer.cpp) |
| 147   | Insertion Sort List                            | Medium     | [C++](147.InsertionSortList.cpp) |
| 148   | Sort List                                      | Medium     | [C++](148.SortList.cpp) |
| 164   | Maximum Gap                                    | Hard       | [C++](164.MaximumGap.cpp) |
| 204   | Count Primes                                   | Easy       | [C++](204.CountPrimes.cpp) |
| 205   | Isomorphic Strings                             | Easy       | [C++](205.IsomorphicStrings.cpp) |
| 217   | Contains Duplicate                             | Easy       | [C++](217.ContainsDuplicate.cpp) |
| 222   | Count Complete Tree Nodes                      | Medium     | [C++](222.CountCompleteTreeNodes.cpp) |
| 283   | Move Zeroes                                    | Easy       | [C++](283.MoveZeroes.cpp) |
| 290   | Word Pattern                                   | Easy       | [C++](290.WordPattern.cpp) |
| 295   | Find Median from Data Stream                   | Hard       | [C++](Offer41.FindMedianFromDataStream.cpp) |
| 297   | Serialize and Deserialize Binary Tree          | Hard       | [C++](Offer37.SerializeAndDeserializeBinaryTree.cpp) |
| 316   | Remove Duplicate Letters                       | Medium     | [C++](316.RemoveDuplicateLetters.cpp) |
| 321   | Create Maximum Number                          | Hard       | [C++](321.CreateMaximumNumber.cpp) |
| 328   | Odd Even Linked List                           | Medium     | [C++](328.OddEvenLinkedList.cpp) |
| 376   | Wiggle Subsequence                             | Medium     | [C++](376.WiggleSubsequence.cpp) |
| 387   | First Unique Character in a String             | Easy       | [C++](387.FirstUniqueCharacterInAString.cpp) |
| 389   | Find the Difference                            | Easy       | [C++](389.FindTheDifference.cpp) |
| 402   | Remove K Digits                                | Medium     | [C++](402.RemoveKDigits.cpp) |
| 435   | Non-overlapping Intervals                      | Medium     | [C++](435.NonOverlappingIntervals.cpp) |
| 452   | Minimum Number of Arrows to Burst Balloons     | Medium     | [C++](452.MinimumNumberOfArrowsToBurstBalloons.cpp) |
| 454   | 4Sum II                                        | Medium     | [C++](454.4SumII.cpp) |
| 455   | Assign Cookies                                 | Easy       | [C++](455.AssignCookies.cpp) |
| 605   | Can Place Flowers                              | Easy       | [C++](605.CanPlaceFlowers.cpp) |
| 621   | Task Scheduler                                 | Medium     | [C++](621.TaskScheduler.cpp) |
| 649   | Dota2 Senate                                   | Medium     | [C++](649.Dota2Senate.cpp) |
| 714   | Best Time to Buy and Sell Stock with Transaction Fee | Medium     | [C++](714.BestTimeToBuyAndSellStockWithTransactionFee.cpp) |
| 738   | Monotone Increasing Digits                     | Medium     | [C++](738.MonotoneIncreasingDigits.cpp) |
| 746   | Min Cost Climbing Stairs                       | Easy       | [C++](746.MinCostClimbingStairs.cpp) |
| 767   | Reorganize String                              | Medium     | [C++](767.ReorganizeString.cpp) |
| 842   | Split Array into Fibonacci Sequence            | Medium     | [C++](842.SplitArrayIntoFibonacciSequence.cpp) |
| 860   | Lemonade Change                                | Easy       | [C++](860.LemonadeChange.cpp) |
| 861   | Score After Flipping Matrix                    | Medium     | [C++](861.ScoreAfterFlippingMatrix.cpp) |
| 922   | Sort Array By Parity II                        | Easy       | [C++](922.SortArrayByParityII.cpp) |
| 946   | Validate Stack Sequences                       | Medium     | [C++](Offer31.ValidateStackSequences.cpp) |
| 973   | K Closest Points to Origin                     | Medium     | [C++](973.KClosestPointsToOrigin.cpp) |
| 976   | Largest Perimeter Triangle                     | Easy       | [C++](976.LargestPerimeterTriangle.cpp) |
| 1030  | Matrix Cells in Distance Order                 | Easy       | [C++](1030.MatrixCellsInDistanceOrder.cpp) |
| 1046  | Last Stone Weight                              | Easy       | [C++](1046.LastStoneWeight.cpp) |
| 1081  | Smallest Subsequence of Distinct Characters    | Medium     | [C++](316.RemoveDuplicateLetters.cpp) |
| 1122  | Relative Sort Array                            | Easy       | [C++](1122.RelativeSortArray.cpp) |
| 1370  | Increasing Decreasing String                   | Easy       | [C++](1370.IncreasingDecreasingString.cpp) |

## 剑指 Offer

| #     | Problem                                        | Difficulty | Link | 
| ----  | ----                                           | ----       | ---- |
| 07    | 剑指 Offer 07. 重建二叉树                        | Medium     | [C++](Offer07.ConstructBinaryTreeFromPreorderAndInorderTraversal.cpp) |
| 12    | 剑指 Offer 12. 矩阵中的路径                      | Medium     | [C++](Offer12.WordSearch.cpp) |
| 13    | 剑指 Offer 13. 机器人的运动范围                   | Medium     | [C++](Offer13.MoveRangeOfRobot.cpp) |
| 16    | 剑指 Offer 16. 数值的整数次方                     | Medium     | [C++](Offer16.MyPow.cpp) |
| 31    | 剑指 Offer 31. 栈的压入、弹出序列                 | Medium     | [C++](Offer31.ValidateStackSequences.cpp) |
| 33    | 剑指 Offer 33. 二叉搜索树的后序遍历序列            | Medium     | [C++](Offer33.BSTPostTraverseSequence.cpp) |
| 35    | 剑指 Offer 35. 复杂链表的复制                    | Medium     | [C++](Offer35.CopyListWithRandomPointer.cpp) |
| 36    | 剑指 Offer 36. 二叉搜索树与双向链表               | Medium     | [C++](Offer36.ConvertBinarySearchTreeToSortedDoublyLinkedList.cpp) |
| 37    | 剑指 Offer 37. 序列化二叉树                      | Hard       | [C++](Offer37.SerializeAndDeserializeBinaryTree.cpp) |
| 38    | 剑指 Offer 38. 字符串的排列  LCOF                | Medium     | [C++](Offer38.StringPermutation.cpp) |
| 41    | 剑指 Offer 41. 数据流中的中位数                   | Hard     | [C++](Offer41.FindMedianFromDataStream.cpp) |
| 45    | 剑指 Offer 45. 把数组排成最小的数                 | Medium     | [C++](Offer45.SortArrayToMinimum.cpp) |

---

